package HDFC;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 * Created by mauris on 21/10/15.
 */
public class HDFCiupdate
{
    public static void main(String[] args)
    {
        try
        {
            FileSystem fs = FileSystem.get(new Configuration());
            long lastTxid = Long.parseLong(args[1]);
            if(fs.exists(new Path(args[0] + "/.lastTxid")))
            {
                fs.delete(new Path(args[0] + "/.lastTxid"), true);
            }

            fs.create(new Path(args[0] + "/.lastTxid/" + lastTxid));

            System.out.println("Updated last processed TXid: " + lastTxid);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
