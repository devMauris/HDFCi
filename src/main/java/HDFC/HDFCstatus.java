package HDFC;

/**
 * Created by mauris on 14/10/15.
 */
import java.io.*;
import java.util.*;
import java.net.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class HDFCstatus
{
    static int CODE_WARNING = 300;
    static int CODE_OK = 200;
    static int CODE_ERR = 400;

    public static String printCode(int exitCode)
    {
        if(exitCode == CODE_OK)
            return"OK";
        if(exitCode == CODE_WARNING)
            return "WARN";
        return "ERROR";
    }
    public static void main(String[] args)
    {
        try
        {
            int exitCode = CODE_OK;
            FileSystem fs = FileSystem.get(new Configuration());
            FileStatus[] pathLs = fs.listStatus(new Path(args[0]));

            String lastFileName = "", firstFileName = ""; //Last file, not starting with '.'

            String maxname = "", minname = ""; //finding min/max filename, because listStatus() doesn't guarantee us right order
            for(int i = 0; i<pathLs.length; i++)
            {
                String filenameURL = pathLs[i].getPath().toString();
                String filename = filenameURL.substring(filenameURL.lastIndexOf('/') + 1);
                if(filename.compareTo(maxname) > 0)
                {
                    maxname = filename;
                }
                if(!filename.startsWith(".") && (filename.compareTo(minname) < 0 || minname.compareTo("") == 0)) //regexp
                {
                    minname = filename;
                }
            }
            lastFileName = maxname;
            firstFileName = minname;
            if(fs.exists(new Path(args[0] + "/.lastprocessed")))
            {
                FileStatus[] lastprocessedLs = fs.listStatus(new Path(args[0] + "/.lastprocessed/"));
                if(lastprocessedLs.length > 0)
                {
                    String firstFileNameURL = lastprocessedLs[0].getPath().toString();
                    firstFileName = firstFileNameURL.substring(firstFileNameURL.lastIndexOf('/') + 1);
                }
                else
                {
                    exitCode = CODE_WARNING; //.lastprocessed is present, no files inside.
                }
            }

            if(firstFileName.compareTo(lastFileName) == 0)  //nothing new was added
                exitCode = CODE_ERR;
            System.out.println(printCode(exitCode) + '\n' + firstFileName + '\n' + lastFileName);


        }
        catch(Exception e)
        {
            System.out.println(args[0]);
            e.printStackTrace();
        }

    }
}
