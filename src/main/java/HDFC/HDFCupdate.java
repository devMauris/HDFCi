package HDFC;

/**
 * Created by mauris on 16/10/15.
 */
import java.io.*;
import java.util.*;
import java.net.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
public class HDFCupdate
{
    static int CODE_WARNING = 300;
    static int CODE_OK = 200;
    static int CODE_ERR = 400;
    public static String printCode(int exitCode)
    {
        if(exitCode == CODE_OK)
            return"OK";
        if(exitCode == CODE_WARNING)
            return "WARN";
        return "ERROR";
    }

    public static void main(String[] args)
    {
        try
        {
            int exitCode = CODE_OK;
            FileSystem fs = FileSystem.get(new Configuration());

            String lastFileName = args[1];
            if(fs.exists(new Path(args[0] + "/.lastprocessed")))
            {
                fs.delete(new Path(args[0] + "/.lastprocessed"), true);
            }

            fs.create(new Path(args[0] + "/.lastprocessed/" + lastFileName));

            System.out.println(printCode(exitCode) + '\n' + "Updated last processed file: " + lastFileName);


        }
        catch(Exception e)
        {
            System.out.println(args[0]);
            e.printStackTrace();
        }
    }
}
