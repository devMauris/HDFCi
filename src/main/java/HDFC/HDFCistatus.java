package HDFC;

/**
 * Created by mauris on 20/10/15.
 */

import java.io.*;
import java.util.*;
import java.net.*;
import java.util.PriorityQueue;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.hdfs.*;
import org.apache.hadoop.hdfs.client.*;
import org.apache.hadoop.hdfs.inotify.*;

public class HDFCistatus
{
    public static void main(String[] args)
    {
        try
        {
            long lastReadTxid = 0;
            long total = 0;

            FileSystem fs = FileSystem.get(new Configuration());
            HdfsAdmin admin = new HdfsAdmin(URI.create(args[0]), new Configuration());
            URL url = new URL(args[0].replace("hdfs", "http"));
            //System.out.println(url.getPath());
            ///  hdfs://addr.ess.com:8020/

            if(fs.exists(new Path(args[0] + "/.lastTxid")))
            {
                FileStatus[] lastprocessedLs = fs.listStatus(new Path(args[0] + "/.lastTxid"));
                if(lastprocessedLs.length > 0)
                {
                    String firstFileNameURL = lastprocessedLs[0].getPath().toString();
                    lastReadTxid = Long.parseLong(firstFileNameURL.substring(firstFileNameURL.lastIndexOf('/') + 1));
                }
                else
                {
                    //Warn: .lastTxid exists, but nothing inside
                }
            }
            else
            {
                //init
                DFSInotifyEventInputStream eventStream = admin.getInotifyEventStream(lastReadTxid);
                EventBatch batch = null;
                while((batch = eventStream.poll()) != null)
                {
                    lastReadTxid = batch.getTxid();
                }

            }
            DFSInotifyEventInputStream eventStream = admin.getInotifyEventStream(lastReadTxid);
            EventBatch batch = null;
            Set<String> updatedDirs = new HashSet<String>();
            while((batch = eventStream.poll()) != null)
            {
                //logic
                total += batch.getEvents().length;
                for (Event event : batch.getEvents())
                {
                    switch (event.getEventType())
                    {
                        case CLOSE:
                            Event.CloseEvent closeEvent = (Event.CloseEvent) event;
                            //System.out.println(closeEvent.getPath());
                            if(closeEvent.getPath().startsWith(url.getPath()))
                            {
                                String dir = closeEvent.getPath().substring(url.getPath().length(), closeEvent.getPath().lastIndexOf('/'));
                                //System.out.println(dir);
                                if(!dir.startsWith(".lastTxid") && dir.matches(args[1]))
                                {
                                    updatedDirs.add(dir);
                                }
                                //updatedDirs.add(closeEvent.getPath().substring(url.getPath().length(), closeEvent.getPath().lastIndexOf('/')));
                            }
                            break;
                        case RENAME:
                            Event.RenameEvent renameEvent = (Event.RenameEvent) event;
                            if(renameEvent.getDstPath().startsWith(url.getPath())) {
                                String dir = renameEvent.getDstPath().substring(url.getPath().length(), renameEvent.getDstPath().lastIndexOf('/'));
                                //System.out.println(dir);
                                if(!dir.startsWith(".lastTxid") && dir.matches(args[1]))
                                {
                                    updatedDirs.add(dir);
                                }
                                //updatedDirs.add(renameEvent.getDstPath().substring(url.getPath().length(), renameEvent.getDstPath().lastIndexOf('/')));
                            }
                            break;
                        default:

                            break;
                    }
                }

                //--------------
                lastReadTxid = batch.getTxid();
            }
            System.out.print(lastReadTxid + " ");
            for(String e : updatedDirs)
            {
                System.out.print(e + " ");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
